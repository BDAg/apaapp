## Bem-vindos ao APaApp

# O que é o APaApp?

APaApp é um aplicativo dedicado ao manejo e identificação de pragas agrícolas através de caracteristicas descritivas fornecidas pelo operador ou pelo próprio agricultor.  

# O que o APaApp oferece?
O APaApp, através das informações gerenciadas pelo usuário, pode trazer uma relação das possíveis pragas que estejam aflingindo a lavoura, e indica quais são suas áreas de atuação.

# Genial! Onde posso adquirir meu APaApp?
O projeto ainda está em desenvolvimento, porém em breve será concluído.

# Quem são os desenvolvedores?
Alunos do 4º termo de Big Data no Agronegócio!
